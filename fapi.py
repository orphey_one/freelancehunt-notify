import base64
import hashlib
import hmac
import urllib.parse
import urllib.request
import urllib.response

TOP_LEVEL_URL = 'https://api.freelancehunt.com/'


# func for signing requests
def sign(api_secret, url, method, post_params=''):
    return base64.standard_b64encode(
        hmac.new(key=api_secret.encode('utf-8'),
                 msg=(url+method+post_params).encode('utf-8'),
                 digestmod=hashlib.sha256).digest()).decode('utf-8')


def api_request(url, api_token, api_secret):
    method = 'GET'
    signature = sign(api_secret=api_secret, url=url, method=method)
    req = urllib.request.Request(url)
    req.add_header('Authorization', 'Basic ' + base64.encodebytes(
        (api_token+':'+signature).encode('utf-8')).decode('utf-8').replace('\n', ''))
    try:
        response = urllib.request.urlopen(req)
        return response.read().decode('utf-8')

    except IOError as e:
        print(e)


def get_feed(api_token, api_secret):
    url = 'https://api.freelancehunt.com/my/feed'
    return api_request(url, api_token, api_secret)