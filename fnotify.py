import gi
import os
import urllib
import fapi
import json
import configparser
import lxml.etree
import webbrowser

gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk
from gi.repository import Notify, GdkPixbuf
from gi.repository import Gio
from gi.repository import GObject


WAIT_TIME = 60
CONFIG_FILE = '~/.config/freelancehunt.conf'
DOMAIN = 'https://freelancehunt.com/'


# TODO: Rewrite code and make it object oriented


class FNotify:
    def __init__(self, text, url, image_url):
        self.notify = Notify.Notification.new('Freelancehunt', text, 'freelancehunt')
        self.url = url
        self.notify.set_app_name('freelancehunt')

        req = urllib.request.Request(image_url,
                                     headers={'User-Agent': 'Freelancehunt Notifier 0.0.1'})

        avatar_response_icon = urllib.request.urlopen(req)
        avatar_input_stream = Gio.MemoryInputStream.new_from_data(avatar_response_icon.read(), None)
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream(avatar_input_stream, None)
        self.notify.set_image_from_pixbuf(pixbuf)

    def refresh(self):
        notify = self.notify
        notify.clear_hints()
        notify.clear_actions()

        try:
            notify.add_action('clicked', 'Show', self.on_notify_show_action, None)
        except TypeError as e:
            print(e)
        notify.show()

    def on_notify_show_action(self, *args):
        webbrowser.open(DOMAIN + self.url)


class ConfigWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Freelancehunt config")
        self.set_size_request(300, 200)
        self.timeout_id = None
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=3)
        self.add(vbox)

        self.api_token_label = Gtk.Label()
        self.api_token_box = Gtk.Entry()

        self.api_secret_label = Gtk.Label()
        self.api_secret_box = Gtk.Entry()

        self.save_button = Gtk.Button.new_with_label('Save')
        self.save_button.connect('clicked', self.on_save_button_clicked)

        self.api_token_label.set_text('API TOKEN:')
        self.api_secret_label.set_text('API SECRET:')
        vbox.pack_start(self.api_token_label, True, True, 0)
        vbox.pack_start(self.api_token_box, True, True, 0)
        vbox.pack_start(self.api_secret_label, True, True, 0)
        vbox.pack_start(self.api_secret_box, True, True, 0)
        vbox.pack_start(self.save_button, True, True, 0)
        self.set_position(Gtk.WindowPosition.CENTER)

    def on_save_button_clicked(self, button):
        config = configparser.ConfigParser()
        file = open(os.path.expanduser(CONFIG_FILE), 'w')
        config['api_keys'] = {'api_token': self.api_token_box.get_text(),
                              'api_secret': self.api_secret_box.get_text()}
        config.write(file)
        file.close()
        Gtk.main_quit()


def message_notify(title, text, image_url):
    url_parse = lxml.etree.HTML(text)
    url = url_parse.xpath("//a")[0].get("href")
    url_text = url_parse.xpath("//a")[0].text
    fnotify = FNotify(text, url, image_url)
    print(url, url_text)
    fnotify.refresh()


def feed_check(token, secret):
    if not Notify.is_initted():
        Notify.init('Freelancehunt')
    json_feed = fapi.get_feed(token, secret)
    try:
        feed = json.loads(json_feed)
        for message in feed:
            if message['is_new']:
                message_notify('Freelancehunt',
                               message['from']['login'] + ' ' + message['message'],
                               message['from']['avatar'])

    except (ValueError, TypeError) as e:
        print(e)
        return


# for test purposes only
#    message = feed[0]
#    message_notify('Freelancehunt',
#                   message['from']['login'] + ' ' + message['message'],
#                   message['from']['avatar'])


def on_gobject_timeout(user_data):
    feed_check(user_data[0], user_data[1])


if __name__ == '__main__':

    config = configparser.ConfigParser()
    api_token = ''
    api_secret = ''
    try:

        if not os.path.isfile(os.path.expanduser(CONFIG_FILE)):
            win = ConfigWindow()
            win.connect("delete-event", Gtk.main_quit)
            win.show_all()
            Gtk.main()
        else:
            config.read_file(open(os.path.expanduser(CONFIG_FILE), 'r'))
            api_token = config['api_keys']['api_token']
            api_secret = config['api_keys']['api_secret']

    except IOError as e:
        print(e)
        os.exit(-1)
    user_data = [api_token, api_secret]

    GObject.timeout_add(WAIT_TIME * 1000, on_gobject_timeout, user_data)
    Gtk.main()
